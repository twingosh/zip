<?php

namespace App;

class Car
{

	/** @var int */
	private $originQueueId;

	/** @var int */
	private $identifier;


	public function __construct($originQueueId, $identifier)
	{
		$this->originQueueId = $originQueueId;
		$this->identifier = $identifier;
	}


	public function getOriginQueueId()
	{
		return $this->originQueueId;
	}

	public function getIdentifier()
	{
		return $this->identifier;
	}

	/** to simplify output */
	public function __toString()
	{
		return $this->originQueueId ."_" . $this->identifier;
	}
}