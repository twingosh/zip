<?php

namespace App;

use App\Queue;
use App\Car;

class Zip
{

	/** @var App\Queue[] */
	private $queues = [];
	/** @var App\Car[] */
	private $finalLine = [];

	/**
	* generates all queues and fill them with cars
	* @var int $lines
	* @var int $cars
	*/
	public function __construct($lines, $cars)
	{
		for($i = 1; $i <= $lines; $i++) {
			$this->queues[$i] = new Queue($i, $cars);
		}
	}

	/**
	* do the zipping magic
	*/
	public function go()
	{
		do {
			$nextCar = null;

			$currentLine = isset($currentLine) && $currentLine < count($this->queues) ? ++$currentLine : 1;
			
			$currentQueue = $this->queues[$currentLine];
			$nextQueue = isset($this->queues[$currentLine+1]) ? $this->queues[$currentLine+1] : null;


			if($nextQueue && !$nextQueue->isEmpty()) {
				if($currentQueue->isWaiting() || $currentQueue->isEmpty()) {
					$nextCar = $nextQueue->getOut();
					if($nextCar) {
						$currentQueue->getIn($nextCar);
					}
				} else if($currentLine == 1) {
					$nextCar = $currentQueue->getOut();
					if($nextCar) $this->finalLine[] = $nextCar;	// going to final line
				}
			} else if($currentLine == 1){
				$nextCar = $currentQueue->getOut();
				if($nextCar) $this->finalLine[] = $nextCar; // going to final lane
			}

			//remove last queue when is empty
			if($currentLine == count($this->queues) && $this->queues[$currentLine]->isEmpty()) {
				unset($this->queues[$currentLine]);
			}
			
		} while (count($this->queues));

		
		//dump final line of cars zipped
		// echo "final line:<br>";
		// foreach($this->finalLine as $car) {
		// 	echo $car . "<br>";
		// }

	return $this->finalLine;

	}
}