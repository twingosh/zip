<?php

namespace App;

use App\Car;

class Queue
{
	/** @var App\Car[] */
	private $cars = [];

	/** @var bool */
	private $isWaiting = false;


	/** 
	* @param int $queueNum
	* @param int $length
	*/
	public function __construct($queueNum, $length)
	{
		for($i=1; $i <= $length; $i++) {
			$this->cars[$i] = new Car($queueNum, $i);
		}
	}

	public function isWaiting() 
	{
		return $this->isWaiting;
	}

	public function getIn(Car $car)
	{	$this->isWaiting = false; // when car is moved to the top of the queue from different one, then car should not wait.
		array_unshift($this->cars, $car);
	}

	public function getOut()
	{
		$this->setWaitingNeed();
		return array_shift($this->cars);
	}


	public function isEmpty()
	{
		return count($this->cars) == 0;
	}


	/**
	* used rule: first round move, second round wait 
	* (valid only for cars that are originally from same queue)
	*/
	private function setWaitingNeed()
	{
		$currentCar = current($this->cars);
		$waitingCar = next($this->cars);
		
		$this->isWaiting = (bool) ($waitingCar && $currentCar->getOriginQueueId() === $waitingCar->getOriginQueueId());		

	}

}