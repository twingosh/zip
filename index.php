<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require "vendor/autoload.php";

use App\Zip;


echo "ZIP APP<br>";

$lines = 6;
$cars = 5;

echo "lines:{$lines}, cars in line:{$cars}<br>";

$zip = new Zip($lines, $cars);
	
$finalLine = $zip->go();

echo "final line:<br>";
foreach($finalLine as $car) {
	echo $car . "<br>";
}