<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

use App\Zip;

class ZipTest extends TestCase

{

	/**
	* @dataProvider resultsProvider
	*/
	public function testResults($line, $carsInLine, $expectedFinalQueue)
	{
		$zip = new Zip($line,$carsInLine);

		$finalQueue = $zip->go();

		$this->assertNotEmpty($finalQueue);


		foreach($finalQueue as $id=>$car)
		{
			$this->assertEquals($expectedFinalQueue[$id],"$car");
		}	

	}

	public function resultsProvider()
	{
		return [
			[1,1,["1_1"]],
			[1,2,["1_1","1_2"]],
			[1,3,["1_1","1_2","1_3"]],
			[2,1,["1_1","2_1"]],
			[2,2,["1_1","2_1","1_2","2_2"]],
			[2,3,["1_1","2_1","1_2","2_2","1_3","2_3"]],
			[3,3,["1_1","2_1","1_2","3_1","1_3","2_2","3_2","2_3","3_3"]],

		];
	}
}